(Scribbled version of C:\Users\ricky\AppData\Local\Temp\ink_ext_XXXXXX.svg3T38K0 @ 3500.00)
( unicorn.py --tab="plotter_setup" --pen-up-angle=50 --pen-down-angle=30 --start-delay=150 --stop-delay=150 --xy-feedrate=3500 --z-feedrate=150 --z-height=0 --finished-height=0 --register-pen=true --x-home=0 --y-home=0 --num-copies=1 --continuous=false --pause-on-layer-change=false C:\Users\ricky\AppData\Local\Temp\ink_ext_XXXXXX.svg3T38K0 )
G21 (metric ftw)
G90 (absolute mode)
G92 X0.00 Y0.00 Z0.00 (you are here)

M300 S30 (pen down)
G4 P150 (wait 150ms)
M300 S50 (pen up)
G4 P150 (wait 150ms)
M18 (disengage drives)
M01 (Was registration test successful?)
M17 (engage drives if YES, and continue)

(Polyline consisting of 1 segments.)
G1 X46.67 Y86.70 F3500.00
M300 S30.00 (pen down)
G4 P150 (wait 150ms)
G1 X46.30 Y85.75 F3500.00
G1 X45.23 Y84.84 F3500.00
G1 X41.11 Y83.13 F3500.00
G1 X34.58 Y81.67 F3500.00
G1 X25.93 Y80.52 F3500.00
G1 X15.84 Y79.81 F3500.00
G1 X5.19 Y79.57 F3500.00
G1 X-5.46 Y79.81 F3500.00
G1 X-15.55 Y80.52 F3500.00
G1 X-24.21 Y81.67 F3500.00
G1 X-30.74 Y83.13 F3500.00
G1 X-34.86 Y84.84 F3500.00
G1 X-35.93 Y85.75 F3500.00
G1 X-36.29 Y86.70 F3500.00
G1 X-35.93 Y87.64 F3500.00
G1 X-34.86 Y88.56 F3500.00
G1 X-30.74 Y90.26 F3500.00
G1 X-24.21 Y91.73 F3500.00
G1 X-15.55 Y92.87 F3500.00
G1 X-5.46 Y93.58 F3500.00
G1 X5.19 Y93.82 F3500.00
G1 X15.84 Y93.58 F3500.00
G1 X25.93 Y92.87 F3500.00
G1 X34.58 Y91.73 F3500.00
G1 X41.11 Y90.26 F3500.00
G1 X45.23 Y88.56 F3500.00
G1 X46.30 Y87.64 F3500.00
G1 X46.67 Y86.70 F3500.00
G1 X46.67 Y86.70 F3500.00
M300 S50.00 (pen up)
G4 P150 (wait 150ms)

(Polyline consisting of 1 segments.)
G1 X46.67 Y10.84 F3500.00
M300 S30.00 (pen down)
G4 P150 (wait 150ms)
G1 X46.31 Y9.90 F3500.00
G1 X45.24 Y8.98 F3500.00
G1 X41.12 Y7.28 F3500.00
G1 X34.59 Y5.81 F3500.00
G1 X25.93 Y4.67 F3500.00
G1 X15.84 Y3.95 F3500.00
G1 X5.19 Y3.71 F3500.00
G1 X-5.46 Y3.95 F3500.00
G1 X-15.55 Y4.67 F3500.00
G1 X-24.20 Y5.81 F3500.00
G1 X-30.73 Y7.28 F3500.00
G1 X-34.85 Y8.98 F3500.00
G1 X-35.92 Y9.90 F3500.00
G1 X-36.29 Y10.84 F3500.00
G1 X-35.92 Y11.78 F3500.00
G1 X-34.85 Y12.70 F3500.00
G1 X-30.73 Y14.40 F3500.00
G1 X-24.20 Y15.87 F3500.00
G1 X-15.55 Y17.01 F3500.00
G1 X-5.46 Y17.73 F3500.00
G1 X5.19 Y17.97 F3500.00
G1 X15.84 Y17.73 F3500.00
G1 X25.93 Y17.01 F3500.00
G1 X34.59 Y15.87 F3500.00
G1 X41.12 Y14.40 F3500.00
G1 X45.24 Y12.70 F3500.00
G1 X46.31 Y11.78 F3500.00
G1 X46.67 Y10.84 F3500.00
G1 X46.67 Y10.84 F3500.00
M300 S50.00 (pen up)
G4 P150 (wait 150ms)

(Polyline consisting of 1 segments.)
G1 X-36.29 Y86.41 F3500.00
M300 S30.00 (pen down)
G4 P150 (wait 150ms)
G1 X-36.29 Y10.29 F3500.00
M300 S50.00 (pen up)
G4 P150 (wait 150ms)

(Polyline consisting of 1 segments.)
G1 X46.67 Y86.84 F3500.00
M300 S30.00 (pen down)
G4 P150 (wait 150ms)
G1 X46.67 Y10.72 F3500.00
M300 S50.00 (pen up)
G4 P150 (wait 150ms)


(end of print job)
M300 S50.00 (pen up)
G4 P150 (wait 150ms)
M300 S255 (turn off servo)
G1 X0 Y0 F3500.00
G1 Z0.00 F150.00 (go up to finished level)
G1 X0.00 Y0.00 F3500.00 (go home)
M18 (drives off)
