#include <iostream>
#include "Gcode_Correction_class.cpp"



using namespace std;

int main() {
    double d = 120, D = 720, l = 400;
    Gcode_Correction_class g = Gcode_Correction_class("test/linea.gcode", d, D, l);
    Coordinate puntoPartenza, puntoCentro, puntoArrivo;
    std::cout << g.modificaGcode() << std::endl;  

    return 0;
}
