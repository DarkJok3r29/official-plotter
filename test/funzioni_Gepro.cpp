#include <iostream>
#include <cmath>
#include <math.h>
#include <fstream>
#include <vector>
#include <algorithm>
#include <ctime>

#define K 0.5

struct Coordinate
{
    double x;
    double y;
};


struct ComandoArchi {
    std::string comando;
    double x;
    double y;
    double i;
    double j;
    double e;
    double f;
};




std::string leggiFileGcode(std::string nomeFile);
double lunghezzaCordaDx(double x, double y, double dx, double dy);
double lunghezzaCordaSx(double D, double x, double y, double dx, double dy);
void segmentazioneSegmentiLineari(double x1, double x2, double y1, double y2);
std::vector<Coordinate> segmentazioneArchiDiCerchio(double xArrivo, double yArrivo, double xCentrale, double yCentrale, double xPartenza, double yPartenza);
std::string modificaGcode(std::string fileGcode);
bool scriviGcode(std::string fileGcode);
void scriviLog(std::string funzione, std::string risultato);


int main() {
    
    std::string fileGcode = leggiFileGcode("gcode.gcode");
    std::string fileModificato = modificaGcode(fileGcode);
    
    if(scriviGcode(fileModificato)) {
        std::cout<< "File Creato - Nuovo GCODE Creato" <<std::endl;
        std::cout<< "Dimensione stringa: " << fileModificato.size() << std::endl;;
    } else {
        std::cout<< "Errore di creazione del file" <<std::endl;
    }
}

//Legge il file che voglio correggere

std::string leggiFileGcode(std::string nomeFile) {
    

    std :: ifstream gcodeFile(nomeFile);
    std :: string line;
    std :: string file = "";

    if(gcodeFile.is_open()) {

        while(std::getline(gcodeFile, line)) {

            file += line + "\n";

        }

        gcodeFile.close();

    } else {
        std::cout << "Non riesco aprire il file";
    }

    return file;
}

bool BothAreSpaces(char lhs, char rhs) { 
    return (lhs == rhs) && (lhs == ' '); 
}

//Corregge il GCODE

std::string modificaGcode(std::string fileGcode) {

    //Legge il numero di linee che ci sono nel gcode
    size_t nLineeGcode = std::count(fileGcode.begin(), fileGcode.end(), '\n');

    std::string lineeGcode[nLineeGcode];

    //Variabile in cui salvo il Gcode Corretto
    std::string fileGcodeModificato = "";

    //Faccio una copia del file per tenere intatto la variabile originale
    std::string copiaFileGcode = fileGcode;

    
    //Tolgo gli spazi multipli
    std::string::iterator new_end = std::unique(copiaFileGcode.begin(), copiaFileGcode.end(), BothAreSpaces);
    copiaFileGcode.erase(new_end, copiaFileGcode.end());  

    //Variabili per splittare la stringa copiaFileGcode
    std::string delimitatore = "\n";
    size_t posizione = 0;
    std::string linea;

    //Divido le linee e le metto nell'array
    int i = 0;
    while((posizione = copiaFileGcode.find(delimitatore)) != std::string::npos) {
        linea = copiaFileGcode.substr(0, posizione);

        lineeGcode[i] = linea;

        copiaFileGcode.erase(0, posizione + delimitatore.length());

        i++;
    }

    //Salvo i comandi G1 e G2 su questo array di strutture
    ComandoArchi comandiFileGcode[nLineeGcode];
    delimitatore = ' ';

    //Salviamo le coordinate precedenti in questa struttura
    Coordinate coordinatePrecedenti;

    //Modifichiamo i comandi G2 e G3 in G1

    for(int i = 0; i < nLineeGcode; i++) {
        linea = lineeGcode[i];
        
        if(linea.find("X") != std::string::npos && linea.find("Y") != std::string::npos) {
            //Prendo il comando
            posizione = linea.find(delimitatore);
            comandiFileGcode[i].comando = linea.substr(0, posizione);
            
            linea.erase(0, posizione + delimitatore.length());    
            //Prendo X
            posizione = linea.find(delimitatore);
            comandiFileGcode[i].x = std::stod(linea.substr(1, posizione));
            linea.erase(0, posizione + delimitatore.length());

            //Prendo Y
            posizione = linea.find(delimitatore);
            comandiFileGcode[i].y = std::stod(linea.substr(1, posizione));
            linea.erase(0, posizione + delimitatore.length());

            coordinatePrecedenti.x = comandiFileGcode[i].x;
            coordinatePrecedenti.y = comandiFileGcode[i].y;

            //Controllo il comando: se è G2 o G3, prendo tutti i valori
            if(comandiFileGcode[i].comando == "G2" || comandiFileGcode[i].comando == "G3") {

                //Prendo I
                posizione = linea.find(delimitatore);
                comandiFileGcode[i].i = std::stod(linea.substr(1, posizione));
                
                linea.erase(0, posizione + delimitatore.length());

                //Prendo J
                comandiFileGcode[i].j = std::stod(linea.substr(1, linea.length()));

                //Creo un vector per salvare i nuovi comandi creato dalla funzione
                std::vector<Coordinate> nuoviComandi;

                //La passo alla funzione segmentazioneArchiDiCerchio se il comando e' G2 o G3
                
                /*
                    In caso il primo comando è proprio un G2/G3 bisogna settare la partenza a 0
                    Oppure quando il comando precedente non ha i valori X e Y (Ancora da fare, non nè ho idea di come fare)
                */
                if(i == 0) {
                    nuoviComandi = segmentazioneArchiDiCerchio(comandiFileGcode[i].x, comandiFileGcode[i].y, comandiFileGcode[i].i, comandiFileGcode[i].j, 0, 0);
                } else {
                    nuoviComandi = segmentazioneArchiDiCerchio(comandiFileGcode[i].x, comandiFileGcode[i].y, comandiFileGcode[i].i, comandiFileGcode[i].j, coordinatePrecedenti.x, coordinatePrecedenti.y);
                }

                //Trasformo tutti i comandi in una stringa con un ciclo sul vector
                //Riscrivo tutto in una stringa 
                for(auto const& value: nuoviComandi) {
                    fileGcodeModificato += "G1 X" + std::to_string(value.x) + " Y" + std::to_string(value.y) + "\n";
                }

            } else {
                fileGcodeModificato += linea + "\n";
            }
        } else {
                fileGcodeModificato += linea + "\n";
        }
 

        /*
            (Questa forse si toglie)
            Serve per vedere se la stringa fileGcodeModificato è minore di 20 righe o uguale
            così da scriverela sul file e svuotare fileGcodeModificato
        */
        /*if(fileGcodeModificato.size() + lineeGcode[i+1].size() > 1574 && i+1 > nLineeGcode) {
            if(scriviGcode(fileGcodeModificato)) {
                fileGcodeModificato = "";
            }
        }*/

    }


    


    //Ritorno la stringa modificata
    return fileGcodeModificato;
}



double lunghezzaCordaDx(double x, double y, double dx, double dy) {
    double ris = sqrt(pow((dx +x), 2) + pow((dy + y), 2));
    return ris;
}

double lunghezzaCordaSx(double D, double x, double y, double dx, double dy) {
    double ris = sqrt((pow(D-dx+x, 2)) + (pow(dy+y, 2)));
    return ris;
}


void segmentazioneSegmentiLineari(double x1, double x2, double y1, double y2) {
    double lunghezzaSegmento = sqrt( (pow((x2-x1), 2)) + (pow(y2-y1, 2)));
    int nSegmenti = floor(lunghezzaSegmento / 5);

    double deltaX = (x2 - x1) / nSegmenti;
    double deltaY = (y2 - y1) / nSegmenti;

    Coordinate c[nSegmenti]; 
    c[0].x = x1;
    c[0].y = y1;

    
    for(int i = 1; i < nSegmenti; i++) {

        
       c[i].x = c[i - 1].x + deltaX;
       c[i].y = c[i -1].y + deltaY;
    }
}

std::vector<Coordinate> segmentazioneArchiDiCerchio(double xArrivo, double yArrivo, double xCentrale, double yCentrale, double xPartenza, double yPartenza) {

    double raggio = sqrt(pow(xCentrale - xPartenza, 2) + pow(yCentrale - yPartenza, 2));
    double lunghezzaCirconferenza = (2 * M_PI) * raggio;

    double alpha = acos(1 - ( K / ( 2 * pow( raggio , 2 ) ) ) );

    double alpha0 = atan( ( yCentrale - yPartenza ) / (xCentrale - xPartenza));


    //QUA CI VA UN IF PER CONTROLLARE SE L'ARCO E' SOPRA IL PUNTO O SOTTO



    //Coordinate Arco sopra al punto

    double xPuntoSuccessivo = ( ( raggio * sin( ( 2 * alpha ) - ( 2 * alpha0) ) / ( 2 * sin(alpha - alpha0 ))) ) + xCentrale ;
    double yPuntoSuccessivo = yCentrale - ( raggio * sin(alpha - alpha0) );

    //Coordinate Arco sotto il punto

    //double x1 = ( ( raggio * sin( ( 2 * alpha ) - ( 2 * alpha0) ) / ( 2 * sin(alpha - alpha0 ))) );
    //double y1 = ( raggio * sin(alpha - alpha0) ) + yCentrale;

    

    double angoloMax = acos( 1 - ( pow( (xCentrale - xPartenza) , 2 ) + pow( (yCentrale - yPartenza) , 2) / 2 ) * (1 / (pow( raggio, 2) )) );

    int ripetizioni = floor(angoloMax / alpha);

    
    if (ripetizioni <= 0){
        Coordinate c[1];
        c[0].x = xArrivo;
        c[0].y = yArrivo;
        std::vector<Coordinate> vectorC(c, c + sizeof(c) / sizeof(Coordinate));


        return vectorC;
    } else{
        Coordinate c[ripetizioni];
        c[0].x = xPuntoSuccessivo;
        c[0].y = yPuntoSuccessivo;

        for(int i = 1; i < ripetizioni; i++) {

            alpha0 = atan( ( yCentrale - yPuntoSuccessivo ) / (xCentrale - xPuntoSuccessivo));

            //QUA CI VA UN IF PER CONTROLLARE SE L'ARCO E' SOPRA IL PUNTO O SOTTO

            xPuntoSuccessivo = ( ( raggio * sin( ( 2 * alpha ) - ( 2 * alpha0) ) / ( 2 * sin(alpha - alpha0 ))) ) + xCentrale ;
            yPuntoSuccessivo = yCentrale - ( raggio * sin(alpha - alpha0) );

            //double x1 = ( ( raggio * sin( ( 2 * alpha ) - ( 2 * alpha0) ) / ( 2 * sin(alpha - alpha0 ))) );
            //double y1 = ( raggio * sin(alpha - alpha0) ) + yCentrale;

            c[i].x = xPuntoSuccessivo;
            c[i].y = yPuntoSuccessivo;
        }
        std::vector<Coordinate> vectorC(c, c + sizeof(c) / sizeof(Coordinate));
        
        return vectorC;
    }
    
}


bool scriviGcode(std::string fileGcode) {
    bool eseguito;
    std::ofstream fileCorretto;
    try {
        fileCorretto.open("fileCorretto.gcode");
        fileCorretto << fileGcode;
        fileCorretto.close();
        eseguito = true;
    } catch(const std::ofstream::failure e) {
        eseguito = false;
    }

    return eseguito;
}



//Log
void scriviLog(std::string funzione, std::string variabile, std::string risultato) {

    

    if(std::ifstream("log.log")) {
        std::cout << "File already exists" << std::endl;

        std::ofstream logFile;

        logFile.open("log.log", std::ios_base::app);

        time_t now = time(0);
        char* datetime = ctime(&now);

        logFile << datetime << " - " << funzione << variabile << " - " << risultato << std::endl;

        logFile.close();
        
    } else {
        std::cout << "File not exists" << std::endl;

        std::ofstream logFile("log.log");


        time_t now = time(0);
        char* datetime = ctime(&now);

        logFile << datetime << " - " << funzione << variabile << " - " << risultato << std::endl;

        logFile.close();

    }

}